extends Sprite3D
var hue_step = 0

func _process(delta):
	if hue_step <= 359 :
		self.set_modulate(Color.from_hsv(hue_step/360,1,1,1))
		hue_step += 5
	else :
		self.set_modulate(Color.from_hsv(hue_step/360,1,1,1))
		hue_step = 0.0
