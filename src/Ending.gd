tool

extends Node2D

export(String) var main_menu

export(OpenSimplexNoise) var noise
export var is_rumbling = false
export var amplitude = 1
var noise_x = 0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _process(delta):
	if !is_rumbling: return
	noise_x += delta
	$Scene/Earth.global_position = Vector2(
		noise.get_noise_2d(noise_x, 0),
		noise.get_noise_2d(0, noise_x) 
		) * amplitude

func rumble():
	pass


func _on_win_pressed():
	Game.next_level()
