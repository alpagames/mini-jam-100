extends Spatial

export(Vector2) var bob_amplitude = Vector2(.2, .2)
# How many bump per second
export(Vector2) var bob_frequency = Vector2(2, .75)
export(bool) var debug = false
onready var player = get_parent()
onready var start_transform = transform
var _cur_bob = Vector2(0,0)

var _bob_offset = Vector2(0,0)

func _ready():
	if not player is Player:
		return

func _process(delta):
	if not player is Player:
		return
	if player.is_on_floor():
		apply_bob(delta)

func apply_bob(delta):
	var vel = player.velocity
	vel.y = 0
	
	var displacement = Vector2(0,0)
	displacement.y = -sin(_bob_offset.y) * bob_amplitude.y
	displacement.x = cos(_bob_offset.x) * bob_amplitude.x
	
	_bob_offset.y += vel.length() * (1/bob_frequency.y) * delta
	_bob_offset.x += vel.length() * (1/bob_frequency.x) * delta
	transform.origin.y = start_transform.origin.y + displacement.y
	transform.origin.x = start_transform.origin.x + displacement.x
	
	if debug:
		DebugDraw.set_text("Bob Offset", _bob_offset)
		DebugDraw.set_text("Displacement", displacement)
		DebugDraw.set_text("Player vel", player.velocity)
		DebugDraw.set_text("Rotation", rotation_degrees.x)
