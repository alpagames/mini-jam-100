extends Sprite

signal splash

var textures = [
	{ "probability": .1, "texture": preload("res://assets/UI/splash-heart-blood.png") },
	{ "probability": .5, "texture": preload("res://assets/UI/splash-left-blood.png") },
	{ "probability": 1, "texture": preload("res://assets/UI/splash-right-blood.png") },
]

func _ready():
	randomize()

func splash():
	if $Anim.is_playing(): return
	emit_signal("splash")
	AudioManager.play("Splash")
	var rnd = randf()
	for tex in textures:
		if rnd <= tex.probability:
			texture = tex.texture
			break
	$Anim.play("Splash")
