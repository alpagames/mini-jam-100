extends DirtyDoggo

func _ready():
	paths = [
		$"Doggo/Face/Dirty",
		$"BackDoggo/Dirty"
	]
	
	tween = $Tween
	
	for sprite in paths:
		print(sprite)
		if sprite:
			sprite.self_modulate.a = 0


func _on_splash():
	print("dirty")

	if tween.is_active():
		tween.stop_all()
	
	for sprite in paths:
		if sprite:
			sprite.self_modulate.a += .5
		
	$DirtyTimer.start()
	



func _on_DirtyTimer_timeout():
	for sprite in paths:
		if sprite:
			tween.interpolate_property(
				sprite,
				"self_modulate",
				sprite.self_modulate,
				Color(1, 1, 1, 0),
				5,
				tween.TRANS_LINEAR
			)
			tween.start()
