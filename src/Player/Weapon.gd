extends Spatial

export(float) var explode_cam_shake
export(float) var charge_decrease_rate
export(float) var min_doggo_height
export(float) var max_doggo_height
export(NodePath) var particles_path
export(NodePath) var face_path
export(NodePath) var bubble_path
export(NodePath) var doggo_path
export(Texture) var sleepy_face_tex
export(Texture) var bubble_tex

export(bool) var debug = true

onready var player : Player = get_parent()
onready var state_machine = $AnimationTree.get("parameters/playback")
onready var particles : Particles2D = get_node(particles_path)
onready var face : Sprite = get_node(face_path)
onready var bubble : Sprite = get_node(bubble_path)
onready var awake_face_tex = face.texture
onready var doggo = get_node(doggo_path)
onready var raycast = player.raycast setget ,get_raycast

# La quantité de charge
var _cur_charge = 100

var _cur_stress = 0

func _ready():
	pass

func _process(delta):
	var rot = -player.head.rotation_degrees.x
	rot = clamp(rot, 0, 60)
	doggo.position.y = lerp(min_doggo_height, max_doggo_height, 1 - rot/60)
	
	var vel = player.velocity
	vel.y = 0
	
	var rate = vel.length()/player._speed
	$AnimationTree.set("parameters/IdleWalk/blend_position", rate)
	
	if not player is Player: return
	if Input.is_action_just_released("shoot"):
		state_machine.travel("IdleWalk")
	if Input.is_action_pressed("shoot") and $ReloadTimer.is_stopped():
		shoot(delta)
		state_machine.travel("Show")
	elif Input.is_action_just_pressed("reload") and $ReloadTimer.is_stopped():
		$ReloadTimer.start()
		state_machine.travel("Pet")
	
	if debug:
		DebugDraw.set_text("Charge", _cur_charge)
		DebugDraw.set_text("BubbleTimer", $BubbleTimer.time_left)
	
	# SHINE
	if particles:
		var new_amount = floor(lerp(0, 20, _cur_charge/100))
		if new_amount == 0:
			new_amount = 1
			particles.emitting = false
		else:
			particles.emitting = true
		if new_amount != particles.amount:
			particles.amount = new_amount
	
	if _cur_charge < .1:
		if face.texture == awake_face_tex:
			face.texture = sleepy_face_tex
			$BubbleTimer.start()
	elif face.texture and face.texture == sleepy_face_tex:
		face.texture = awake_face_tex
		$BubbleTimer.stop()
		if bubble:
			bubble.visible = false
		
		
func shoot(delta):
	if _cur_charge <= 0: 
		_cur_charge = 0
		return
	get_raycast()
	if raycast.is_colliding():
		var o = raycast.get_collider()
		_cur_charge -= charge_decrease_rate * delta
		_cur_stress += delta
		if o && o.has_method("on_hit"):
			o.call("on_hit", raycast.get_collision_normal())
	else:
		_cur_stress -= delta*10
		_cur_stress = 0 if _cur_stress < 0 else _cur_stress
	player.cam.add_stress(_cur_stress)


func pet_sound():
	AudioManager.play("PetDoggo")


func explode():
	player.cam.add_stress(explode_cam_shake)


func _on_UsedTimer_timeout():
	pass # Replace with function body.


func _on_ReloadTimer_timeout():
	_cur_charge = 100


func _on_BubbleTimer_timeout():
	if not bubble: return
	bubble.visible = true

func get_raycast():
	raycast = player.raycast
