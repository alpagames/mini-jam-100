extends Control

export(NodePath) var position_path
onready var offset = rect_position

onready var position = get_node(position_path)

var hue_step = 0.0
var life_percent = 0.0

func _process(delta):
	DebugDraw.set_text("Unprojected Pos", Game.player.cam.unproject_position(position.transform.origin))
	visible = not Game.player.cam.is_position_behind(position.global_transform.origin)
	rect_position = offset + Game.player.cam.unproject_position(position.global_transform.origin)
	rect_position.x = clamp(rect_position.x, -offset.x/2, 10000)
	rect_position.y = clamp(rect_position.y, -offset.y/2, 10000)
	
	# Couleur de la barre
	if hue_step <= 359 :
		$CutnessOverload/Label.set_modulate(Color.from_hsv(hue_step/360,life_percent/100,1,1))
		hue_step += 5
	else :
		$CutnessOverload/Label.set_modulate(Color.from_hsv(hue_step/360,life_percent/100,1,1))
		hue_step = 0.0

func update_progress_bar(value):
	life_percent = value
	$CutnessOverload/Label.text = (String(int(round(value))) + " / 100 % overloaded")
	$CutnessOverload.value = value

func _on_near_body_entered(body):
	if body is Player:
		$CutnessOverload.visible = true


func _on_near_body_exited(body):
	if body is Player:
		$CutnessOverload.visible = false


func _on_very_near_entered(body):
	if body is Player:
		$Panel.visible = true

func _on_VeryNearZone_body_exited(body):
	if body is Player:
		$Panel.visible = false

func talk(value):
	$Panel/Label.text = String(value)
