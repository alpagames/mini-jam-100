extends KinematicBody
class_name Pegu

onready var pegu_ui = $CanvasLayer/PeguUI

var data = {
	max_life = 1000.0,
	current_life = 1000.0,
	screams = [
		"AAAAAAAAAAAAAH",
		"Ooh... He.. Hoo! HAA! HAAAAAA!",
		"Oh My God!",
		"The cuteness... it's overwhelming!",
		"I can't believe this!!!",
		"WTF",
		"OOF",
		"AAAaaargh!!",
		"Holly molly!",
		"What is happening?",
		"It tickles a little too much!",
		"I'm vomiting rainbows?!!",
		"hahaha.. haha.. ah.. ha! HA! HAAAAAAAA!",
		"My body hurts!!!",
		"My body tells me no but my eyes tell me YES!!",
		"Their cuteness level is over 9000!",
		"A AA \n AA AH",
		"CUTENESS OVERLOAD"
	],
	objections = [
		"You are scary, I don't want you here!",
		"Nonononononononononono",
		"Hello again.",
		"Leave me alone!!",
		"How about NO.",
		"Your animal is TOO cute, get lost!",
		"Are you going to hurt me again?",
		"Is it going to be painful again?"
	],
	speed = 1.5
}

var velocity = Vector3(0,0,0)

export var stopped = false

export var dance = false

var is_being_hit = false
var has_been_attacked = false

var regaining_life = false

onready var gender = true if randf() < .5 else false

export(PackedScene) var explosion

onready var modifiers = PeguMod.get_modifier()

func _ready():
	if modifiers.id == "cat_enjoyer" || modifiers.id == "roger" :
		$Sprite3D.set_texture(modifiers.sprite)
	
	$Sprite3D.modulate = modifiers.color
	
	regain_timer_reset()
	$WalkTimer.wait_time = set_rnd(2,5)
	if stopped || dance : $WalkTimer.stop()
	$TurnTimer.wait_time = set_rnd(2,5)
	if stopped || dance: $TurnTimer.stop()
	if dance :
		$Anim.play("Dance")
		$Sprite3D.set_frame(int(set_rnd(0,4)))

func _process(delta):
	# Annuler l'animation de Hit
	if !is_being_hit && $HitAnim.current_animation == "IsHit":
		AudioManager.stop("Man" if gender else "Woman")
		$Sprite3D.set_hit(false)
		$HitAnim.play("NoHit")
		$Particles.set_emitting(false)
		$RegainTimer.start()
		begin_fleeing()
	
	# gestion du regain de vie
	if regaining_life :
		if(data.current_life < data.max_life):
			data.current_life = data.current_life + modifiers.increase_rate
			pegu_ui.update_progress_bar(100.0-(data.current_life/data.max_life*100.0))
		elif (data.current_life >= data.max_life) :
			regaining_life = false
			data.current_life = data.max_life
	
	# mouvement du PEGU
	velocity = Vector3(0,0,0)
	velocity.y -= 10
	if !stopped:
		velocity = global_transform.basis.z * data.speed
		if !$Anim.current_animation == "Walk":
			$Anim.play("Walk")
	elif $Anim.current_animation == "Walk":
		$Anim.play("Stand")
	velocity = move_and_slide(velocity)
	
	is_being_hit = false

func set_new_direction():
	var rnd = set_rnd(0,360)
	self.rotation_degrees = Vector3(0,rnd,0)

#se faire tirer dessus
func on_hit(raycast_normal):
	$Sprite3D.set_hit(true)
	is_being_hit = true
	dance = false
	regaining_life = false
	has_been_attacked = true
	stopped = true
	data.current_life = data.current_life - modifiers.decrease_rate
	pegu_ui.update_progress_bar(100.0-((data.current_life/data.max_life)*100))
	if data.current_life <= 0 :
		die()
		return
	
	if !$HitAnim.current_animation == "IsHit":
		$Anim.play("RESET")
		$HitAnim.play("IsHit")
		$Particles.set_emitting(true)
		regain_timer_reset()
		var rnd = set_rnd(0, data.screams.size())
		pegu_ui.talk(data.screams[rnd])
		AudioManager.play("Man" if gender else "Woman")

func die():
	AudioManager.play("ExplosionBody")
	AudioManager.play("ExplosionSparkles")
	Game.player.update_score(modifiers.score)
	
	var scene_instance = explosion.instance()
	scene_instance.set_name("ExplosionManager")
	scene_instance.transform.origin = self.transform.origin
	scene_instance.make_boom()
	get_parent().add_child(scene_instance)
	self.queue_free()

func _on_RegainTimer_timeout():
	regain_timer_reset()
	regaining_life = true

func regain_timer_reset():
	$RegainTimer.stop()
	$RegainTimer.wait_time = modifiers.wait_time

func _on_VeryNearZone_body_entered(body):
	if not body is Player: return
	randomize()
	if(has_been_attacked):
		var rnd = set_rnd(0, data.objections.size())
		pegu_ui.talk(data.objections[rnd])
	else:
		AudioManager.play("HmMan" if gender else "HmWoman")
		var rnd = set_rnd(0, modifiers.texts.size())
		pegu_ui.talk(modifiers.texts[rnd])

func _on_WalkTimer_timeout():
	$WalkTimer.wait_time = set_rnd(2, 10)
	stopped = !stopped
	$WalkTimer.start()

func _on_TurnTimer_timeout():
	set_new_direction()
	$TurnTimer.wait_time = set_rnd(2, 10)
	$TurnTimer.start()

func set_rnd(v_min,v_max):
	randomize()
	var rnd = rand_range(v_min,v_max)
	rnd = floor(rnd)
	return rnd

func begin_fleeing():
	$TurnTimer.stop()
	$WalkTimer.stop()
	stopped = false
	data.speed = 3
	$Anim.playback_speed = 3
	self.look_at(Game.player.global_transform.origin, Vector3.UP)
