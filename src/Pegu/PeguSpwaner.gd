extends Spatial

var cooled_down = true
var can_spawn = true
var blocker = []

export(PackedScene) var pegu

func _process(delta):
	if can_spawn && blocker.size()==0 && cooled_down:
		cooled_down = false
		var scene_instance = pegu.instance()
		scene_instance.set_name("Pegu")
		scene_instance.transform.origin = self.transform.origin
		get_parent().add_child(scene_instance)
		$Cooldown.start()


func _on_Radar_body_entered(body):
	if body is Player : can_spawn = false


func _on_Radar_body_exited(body):
	if body is Player : can_spawn = true


func _on_Blocker_body_entered(body):
	if body is Pegu : blocker.append(body)


func _on_Blocker_body_exited(body):
	if body is Pegu : blocker.erase(body)


func _on_Cooldown_timeout():
	cooled_down = true
