extends Sprite3D

var data = {
	
	}

export var anim_col = 0

onready var player = Game.player

var is_being_hit = false

func _process(delta):
	var p_vector_j = (self.global_transform.origin - player.global_transform.origin).normalized()
	
	var self_forward = global_transform.basis.z
	var self_left = global_transform.basis.x
	
	var left_dot = self_left.dot(p_vector_j)
	var forward_dot = self_forward.dot(p_vector_j)
	
	var needed_frame = 0
	
	if !self.get_parent().dance : flip_h = false
	if forward_dot < -0.1 :
		needed_frame = 0 # face
	elif forward_dot > 0.1:
		needed_frame = 1 # arrière
	else : 
		if left_dot  > 0 :
			needed_frame = 2 # gauche
			if !self.get_parent().dance : flip_h = true
		else :
			needed_frame = 2 # droite
	
	if !is_being_hit:
		self.frame_coords.y = needed_frame

func set_hit(value):
	is_being_hit = value
