extends Spatial


func make_boom():
	for child in self.get_children():
		if child is Particles : child.set_emitting(true)
	$Timer.start()


func _on_Timer_timeout():
	self.queue_free()
