extends Node

var weapon = "Dog"

var player : Player

var levels = [
	"res://src/UI/MainMenu.tscn",
	"res://src/Levels/Level0.tscn",
	"res://src/Levels/Level1.tscn",
	"res://src/Ending.tscn"
]

var cur_level = -1

func next_level():
	cur_level += 1
	if cur_level > 3:
		cur_level = 0
	SceneManager.change_scene(
		levels[cur_level],
		{ "pattern": "scribbles", "pattern_leave": "scribbles" })
