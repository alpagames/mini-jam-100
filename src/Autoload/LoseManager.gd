extends Control

export(String) var game_scene

var is_lost = false

func _ready():
	SceneManager.connect("fade_complete", self, "hide")

func _on_lost():
	$Anim.play("Lose")
	get_tree().paused = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$CanvasLayer/Control/Button.mouse_filter = Control.MOUSE_FILTER_STOP

func hide():
	$Anim.play("RESET")
	$CanvasLayer/Control/Button.mouse_filter = Control.MOUSE_FILTER_IGNORE
	visible = false

func _on_retry_pressed():
	Game.cur_level = -1
	Game.next_level()
