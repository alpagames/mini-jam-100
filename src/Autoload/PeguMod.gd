extends Node

var modifiers = [
	{
		"id" : "cat_enjoyer",
		"texts": [
			"I like cats better",
			"What is this?",
			"I'm allergic to dogs",
			"Am I supposed to like it?",
			"Dogs are overrated",
			"What is it gonna do? Stink on me?"
		],
		"decrease_rate": 10.0,
		"wait_time": 3,
		"increase_rate": 10.0,
		"score": 10,
		"sprite": preload("res://assets/Pegu/sprite-sheet-cat.png"),
		"color": Color(1, 0, 0)
	},
	
	{
		"id": "nothing_enjoyer",
		"texts": [
			"I guess it's kinda cute",
			"I'm not sure if I'm a dog person",
			"It's like a baby, but fluffier",
			"What's his name?"
		],
		"decrease_rate": 30.0,
		"wait_time": 2,
		"increase_rate": 10.0,
		"score": 5,
		"color": Color(1, .5, .5)
	},
	
	{
		"id": "dog_enjoyer",
		"texts": [
			"It's so cute!",
			"I want to pinch its cheeks",
			"It's so fluffy!",
			"Can I hold it?"
		],
		"decrease_rate": 50.0,
		"wait_time": 2,
		"increase_rate": 5.0,
		"score": 5,
		"color": Color(1, 1, 1)
	},
	{
		"id": "roger",
		"texts": [
			"I am a Roger. Hello",
			"Hello I am an easter egg.",
			"Don't tell the dev leader I am here.",
			"I am Roger, I used to be a snake."
		],
		"decrease_rate": 50.0,
		"wait_time": 0,
		"increase_rate": 1.0,
		"score": 50,
		"sprite": preload("res://assets/Pegu/sprite-sheet-roger.png"),
		"color": Color(1, 1, 1)
	},
]

func get_modifier():
	randomize()
	var rnd = rand_range(0, 100)
	rnd = floor(rnd)
	var i = 2 #dog_enjoyer
	if rnd <= 60 :
		i = 1 #neutral
		if rnd <= 20 :
			i = 0 #cat_enjoyer
		elif rnd <= 1 :
			i = 3 #roger
	return modifiers[i]
