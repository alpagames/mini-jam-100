extends Node

func _ready():
	SceneManager.connect("fade_complete", self, "_on_scene_change")

func _on_scene_change():
	for child in get_children():
		if child is AudioStreamPlayer:
			child.stop()

func play(sound : String):
	var node = get_node(sound)
	if node and node is AudioStreamPlayer:
		node.play()

func stop(which : String):
	if not which: return
	var node = get_node(which)
	if node and node is AudioStreamPlayer:
		node.stop()

func playing(sound: String):
	var node = get_node(sound)
	if node and node is AudioStreamPlayer:
		return node.playing
	return false
