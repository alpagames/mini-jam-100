extends AudioStreamPlayer

func play(val : float = 0):
	var rnd = floor(rand_range(0, get_child_count()))
	var child = get_child(rnd)
	for c in get_children():
		c.stop()
	if child is AudioStreamPlayer or child is AudioStreamPlayer2D:
		child.play(val)
