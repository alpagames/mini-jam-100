tool

extends Spatial

export(bool) var generate_trimesh setget generate
export(bool) var extract_meshes setget extract_meshes_action

func generate(val):
	if val:
		for child in get_children():
			if child is MeshInstance:
				child.create_trimesh_collision()
			elif child.get_child(0) and child.get_child(0) is MeshInstance:
				child.create_trimesh_collision()

func extract_meshes_action(val):
	if val:
		var children = get_children()
		for node in children:
			var mi = find_meshinstance(node)
			if mi and mi is MeshInstance:
				var new = mi.duplicate()
				add_child(new)
				new.set_owner(get_tree().get_edited_scene_root())
				node.queue_free()

func find_meshinstance(node):
	for child in node.get_children():
		if child is MeshInstance:
			return child
		elif child.get_child_count():
			return find_meshinstance(child)
