extends Control

export(String) var main_menu

func _ready():
	OS.set_window_maximized(true)

func next():
	Game.next_level()
