extends VBoxContainer

var tutorial_step = 0

func _ready():
	tutorial()

func _process(delta):
	if tutorial_step == 0 and Input.is_action_just_pressed("mouse_input"):
		yield(get_tree().create_timer(1), "timeout")
		if Input.is_action_pressed("mouse_input"):
			tutorial_step += 1
			tutorial()
	
	if tutorial_step == 1 and Input.is_action_just_pressed("reload"):
		tutorial_step += 1 
		tutorial()
	
	if tutorial_step == 2:
		tutorial_step += 1
		yield(get_tree().create_timer(5), "timeout")
		tutorial()
		set_process(false)

func tutorial():
	match tutorial_step:
		0:
			$Click.visible = true
		1: 
			$Click.visible = false
			$Reload.visible = true
		2:
			$Reload.visible = false
			$Goal.visible = true
		3: 
			$Goal.visible = false
