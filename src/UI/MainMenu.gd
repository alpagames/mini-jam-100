extends Node

func _ready():
	Game.cur_level = 0
	AudioManager.play("MenuMusic")
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func play():
	#print("OK")
	$Popup.popup()

func start():
	Game.next_level()


func _on_Button_Duck_pressed():
	Game.weapon = "Duck"
	start()


func _on_Button_Doggo_pressed():
	Game.weapon = "Dog"
	start()


func _on_Button_Quit_pressed():
	get_tree().quit()
