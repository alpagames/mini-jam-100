extends Control

export(float) var time_before_decrease = 1
export(float) var decrease_rate # Per Second
export(bool) var is_decreasing

onready var tween : Tween = $Tween
onready var broken_bar = $ProgressBar/BrokenBar

var is_overloaded = false setget set_is_overloaded

func _process(delta):
	check_overload()
	
	if is_overloaded and Input.is_action_just_pressed("next"):
		Game.next_level()
	
	if is_overloaded: return
	
	if is_decreasing:
		$ProgressBar.value -= decrease_rate * delta
	if $Anim.is_playing() and $Anim.current_animation == "Add": return
	
	if $ProgressBar.value < 30 and $Anim.current_animation != "LowScore":
		$Anim.play("LowScore")
	if $ProgressBar.value > 30 and $Anim.current_animation == "LowScore" and $Anim.is_playing:
		$Anim.stop()
		$Anim.current_animation_position = 0
	


func add_score(value, stop_decrease=false):
	if tween.is_active():
		tween.stop($ProgressBar)
	tween.interpolate_property(
		$ProgressBar, 
		"value", 
		$ProgressBar.value, 
		$ProgressBar.value + value, 
		.5, 
		tween.TRANS_BOUNCE,
		tween.EASE_OUT)
	tween.start()
	$Anim.play("Add")
	
	if not stop_decrease:
		is_decreasing = false
		$DecreaseTimer.wait_time = time_before_decrease
		$DecreaseTimer.start()

func check_overload():
	if $ProgressBar.value <= 0:
		set_process(false)
		LoseManager._on_lost()
		
	
	if $ProgressBar.value >= 100:
		set_is_overloaded(true)
	else:
		set_is_overloaded(false)

func set_is_overloaded(val):
	broken_bar.visible = val
	if not is_overloaded:
		if val: 
			$Shatter.emitting = true
			AudioManager.play("BrokenGlass")
	if is_overloaded:
		$Tutorial/Next.visible = true
	is_overloaded = val
	

func set_score(value, stop_decrease=false):
	if tween.is_active():
		tween.stop($ProgressBar)
	tween.interpolate_property(
		$ProgressBar, 
		"value", 
		$ProgressBar.value, 
		$ProgressBar.value + value, 
		.5, 
		tween.TRANS_BOUNCE,
		tween.EASE_OUT)
	tween.start()
	$Anim.play("Add")
	
	if not stop_decrease:
		is_decreasing = false
		$DecreaseTimer.wait_time = time_before_decrease
		$DecreaseTimer.start()


func _on_DecreaseTimer_timeout():
	is_decreasing = true
